
DHCP_INF=enp0s3
DHCP_NETWORK=10.4.0.0
DHCP_SRV=10.4.0.1
DHCP_NETMASK=255.255.0.0
TFTP_SRV=10.4.0.1
NAME_SRV=10.4.0.1
DHCP_IP_RANGE=10.4.248.1-10.4.255.254
OS_ISO=/root/CentOS-7-x86_64-DVD-1611.iso
BMC_USER=hpc
BMC_PASS=hpc
BMC_NETWORK=10.4.128.0
domain_name=cep.kr
MAX_SERVERS=20

error_exit() {
   echo $*
   exit 1
}

[ -f lib/klib.so ] || error_exit "klib.so file not found"
. lib/klib.so



base_image_str=$(tabdump osimage | sed "s/\"//g" | awk -F, '{if($6=="install") printf "%s,%s", $1,$13}')
base_image=$(echo $base_image_str | awk -F, '{print $1}')
base_arch=$(echo $base_image_str | awk -F, '{print $2}')


for ((srv_snum=1; srv_snum<=$MAX_SERVERS; srv_snum++)); do
   srv_mac="00:00:00:00:00:00"
   srv_info=$(grep "^${srv_snum}|" server_list.cfg | awk -F\| '{print $2}')
   [ -n "$srv_info" ] && srv_mac=$srv_info
   echo $srv_snum $srv_mac

   mkdef -t node server-$(printf "%04d" $srv_snum) groups=all,servers arch=$base_arch bmc=$(_k_net_add_ip $BMC_NETWORK $srv_snum) bmcusername=$BMC_USER bmcpassword=$BMC_PASS mac=$srv_mac mgt=ipmi netboot=xnba provmethod=$base_image

done

