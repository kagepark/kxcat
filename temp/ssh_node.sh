node=$1

error_exit() {
   echo $*
   exit 1
}

[ -n "$node" ] || error_exit "$(basename $0) <node name>"

find2() {
  local line host_mac
  host_mac=$1
  dhcp_tmp=/var/lib/dhcpd/dhcpd.leases

  line=$(grep -n "$host_mac" $dhcp_tmp | awk '{print $1}' | sed "s/://g" | tail -n 1)
  [ -n "$line" ] || error_exit "IP not found"
  sed -n "$(($line-15)),$line p" $dhcp_tmp | grep "^lease" |tail -n 1| awk '{print $2}'
}

host_mac=$(lsdef $node | grep mac | awk -F= '{print $2}')
node_ip=$(find2 $host_mac)

[ -n "$node_ip" ] && ssh $node_ip
