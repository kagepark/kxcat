node=$1
eth0_mac=$2
bmc_ip=$3
bmc_user=$4
bmc_pass=$5
base_image=$6
base_arch=$7

error_exit() {
   echo $*
   exit
}

[ -n "$node" ] || error_exit "$(basename $0) <node name> <eth0 mac> <bmc ip> <bmc_user> <bmc pass> <base_image> <base_arch>"

mkdef -t node $node groups=all,$base_arch arch=$base_arch bmc=$bmc_ip bmcusername=$bmc_user bmcpassword=$bmc_pass mac=$eth0_mac mgt=ipmi netboot=xnba provmethod=$base_image
