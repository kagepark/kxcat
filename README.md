# KxCAT


Based HPC engine is xCAT(Opensource from IBM)
This KxCAT is user friendly interface for xCAT.
KxCAT has enhanced HPC structure based on xCAT design.
 - more user friendly CLI interface
 - more easily usable CLI
   + Simple CLI commands run grouped progress according to simplified HPC structure.
   + reduce user's mistake.
   + anybody can handle whole HPC system without many knowledge.
 - making simple HPC structure from original xCAT structure
 - Support
   + Multi OS (CentOS, RHEL, ...)
   + Multi type of HPC (Diskless, Diskful)



KxCAT environment file : /etc/profile.d/kxcat.sh
command : kxcat
- I changed command from kxcat to sce
- I have a new job in Supermicro Computer and I changed it

DB File:
<KxCAT HOME>/etc/kxcat.cfg

Install:
<KxCAT HOME>/sbin/install_kxcat.sh

Help: 
 - it will keep changing command to grouped function and descriptions
 - This will be initial command and descriptions

$ sce help

# Display information
servers       # shows server list
clusters      # Show group list for Cluster
images        # Show base image list

#Images
create_image <iso file>      # build image from ISO file
put image <image name>       # update base OS image's information
get image [<image name>]     # Get base OS image's information


# cluster
start          # start cluster
stop           # stop cluster
clone          # clone cluster
create         # create cluster
delete         # create cluster
checkout       # create cluster
checkin        # create cluster


#Host
hosts          # create cluster
put node       # update node's information
get node       # Get node's information
updatenode     # Update information to host
update         # Update information to host
clone_host <base host> <hostname> <ip> <mac> # clone to host from <base host> template

#MISC
console        # ompute node's console
power          # compute node's power handle

#tools
ssh            # create cluster
scp            # create cluster
tool           # create cluster
backup         # Backup DB
restore        # Restore DB

#Logs
Look /var/log/xcat directory

